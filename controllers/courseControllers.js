const Course = require("../models/Course.js");
const auth = require("../auth.js");

/* 
    Steps:
    1. Create a new Course object using the mongoose model and information from the request of the user.
    2. Save the course to the database
*/
module.exports.addCourse = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    console.log(userData);

    let message = "";
    if (!userData.isAdmin) {
        message = `You are not permitted to add courses`;
        return response.send(message);
    } else {
        let newCourse = new Course({
            name: request.body.name,
            description: request.body.description,
            price: request.body.price,
            slots: request.body.slots,
        });

        return newCourse
            .save()
            .then((result) => {
                console.log(result);
                response.send(true);
            })
            .catch((error) => {
                console.log(error);
                response.send(false);
            });
    }
};
