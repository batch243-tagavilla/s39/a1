const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// check if email already exists
/* 
    Steps:
    1. Use mongoose "find" method to find duplicate emails
    2. Use the "then" method to send a response to the frontend application based on the result of the find method
*/
module.exports.checkEmailExists = (request, response, next) => {
    // The result is sent back to the front end via the then method
    return User.find({ email: request.body.email }).then((result) => {
        let message = "";
        // the find method returns an array record of matching documents
        if (result.length > 0) {
            message = `The ${request.body.email} is already taken, please use other email.`;
            return response.send(message);
        } else {
            // message = `The email: ${request.body.email} is not yet taken.`;
            // return response.send(message);
            next();
        }
    });
};

module.exports.registerUser = (request, response) => {
    // create a variable "newUser" and instantiates a new 'User' object using mongoose model
    // uses the information from request body to procide the neccessary information
    let newUser = new User({
        firstName: request.body.firstName,
        lastName: request.body.lastName,
        email: request.body.email,
        /* salt round that bcrypt algorithm will run to encrypt the password */
        password: bcrypt.hashSync(request.body.password, 10),
        mobileNo: request.body.mobileNo,
    });

    return newUser
        .save()
        .then((user) => {
            console.log(user);
            response.send(
                `Congratulations Sir/Ma'am ${newUser.firstName}! You are now registered.`
            );
        })
        .catch((error) => {
            console.log(error);
            response.send(
                `Sorry, ${newUser.firstName}, there was an error during registration. Please try again!`
            );
        });
};

// User authentication
/* 
    Steps:
    1. Check if the user email exist
    2. Compare the password provided in the login form with the password stored in the database
*/
module.exports.loginUser = (request, response) => {
    // the findOne method, returns the first record in the collection that matches the search criteria

    return User.findOne({ email: request.body.email }).then((result) => {
        if (result === null) {
            response.send(
                `Your email ${request.body.email} is not yet registered. Register first!`
            );
        } else {
            // creates the variable "isPasswordCorrect" tp return the result of comparing the login form password and the database password
            // the compareSync method is used to compare a non encrypted password from the login form to the encrypted password retrieved. it will return true or false value depending on the result
            const isPasswordCorrect = bcrypt.compareSync(
                request.body.password,
                result.password
            );

            if (isPasswordCorrect) {
                let token = auth.createAccessToken(result);
                console.log(token);
                return response.send({
                    accessToken: token,
                });
            } else {
                return response.send(`Incorrect password. Please try again.`);
            }
        }
    });
};

/* ACTIVITY */

module.exports.getProfile = (request, response) => {
    return User.findById(request.body.id)
        .then((result) => {
            result.password = "******";
            console.log(result);
            return response.send(result);
        })
        .catch((error) => {
            console.log(error);
            return response.send(error);
        });
};

module.exports.profileDetails = (request, response) => {
    // userData will be object that contains id and email of the user that is currently logged in
    const userData = auth.decode(request.headers.authorization);
    console.log(userData);
    return User.findById(userData.id)
        .then((result) => {
            result.password = "*Confidential*";
            return response.send(result);
        })
        .catch((err) => {
            console.log(err);
            return response.send(err);
        });
};
