const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseControllers.js");

// Adding course
router.post("/", courseController.addCourse);

module.exports = router;
